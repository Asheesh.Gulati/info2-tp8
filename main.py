import math
import os
import re

authors = []  # cette liste contient les noms de famille des auteurs présents dans le corpus d'entraînement

author_features = {}  # ce dictionnaire contient une entrée par auteur, dont la valeur est un dictionnaire de n-grammes
author_feature_counts = {}  # ce dictionnaire contient une entrée par auteur, dont la valeur est le compte total des n-grammes pour cet auteur

feature_vocabulary = set()  # cet ensemble contient tous les n-grammes apparaissant dans le corpus d'entraînement


def list_filepaths(folder):
    return [os.path.join(folder, f) for f in os.listdir(folder) if os.path.isfile(os.path.join(folder, f))]


def load_text(filepath):
    with open(filepath, 'r', encoding='utf8') as f:
        return re.sub(r'^[\S\s]+[*]{3} START OF .+[*]{3}([\S\s]+)[*]{3} END OF .*[*]{3}[\S\s]+$', r'\1', f.read())


def extract_ngrams(filepath, n=1):
    ngrams = []
    text = load_text(filepath)
    # --- TODO 1
    for line in text.strip().split('\n'):
        line = line.strip()
        for i in range(len(line) - n + 1):
            ngrams.append(line[i:i + n])
    # ---
    return ngrams


def record_features(features, author):
    # --- TODO 2
    for f in features:
        author_features[author][f] = author_features[author].get(f, 0) + 1
        author_feature_counts[author] += 1
        if f not in feature_vocabulary:
            feature_vocabulary.add(f)
    # ---


def train(author, n=1):
    authors.append(author)
    author_features[author] = {}
    author_feature_counts[author] = 0
    for filepath in list_filepaths(os.path.join('data', 'train', author)):
        ngrams = extract_ngrams(filepath, n)
        record_features(ngrams, author)


def predict(filename, n=1):
    vocabulary_size = len(feature_vocabulary)

    ngrams = extract_ngrams(os.path.join('data', 'test', filename), n)
    predictions = {}  # ce dictionnaire contient une entrée par auteur, dont la valeur est la probabilité a posteriori
    for author in authors:
        # --- TODO 3
        probability = math.log(1 / len(authors))

        for ngram in ngrams:
            probability += math.log(
                (author_features[author].get(ngram, 0) + 1) / (author_feature_counts[author] + vocabulary_size))

        predictions[author] = probability
        # ---

    return max(predictions, key=predictions.get)


if __name__ == '__main__':
    n = 2  # n = 1 pour unigrammes, n = 2 pour bigrammes, etc.

    for author in os.listdir(os.path.join('data', 'train')):
        train(author, n)

    for filename in os.listdir(os.path.join('data', 'test')):
        print(f'Prediction for \'{filename}\':', predict(filename, n))
